/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 1200;
    int screenHeight = 700; 
    char windowTitle[30] = "FINAL PONG BY XAVI";
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    Rectangle player;
    player.width = 20;
    player.height = 80;
    player.x = 20;
    player.y = screenHeight/2 - player.height/2;
    int playerSpeedY = 9.7;
    
    
    Rectangle enemy;
    enemy.width = 20;
    enemy.height = 80;
    enemy.x = screenWidth - enemy.width - 20;
    enemy.y = screenHeight/2 - player.height/2;
    int enemySpeedY = 8;
    
    Vector2 ballPosition = { screenWidth/2, screenHeight/2 };
    Vector2 ballSpeed = {9, 10};
    int ballRadius = 20;

    int secondsCounter = 99;
    
    int framesCounter;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    Rectangle playerBar;
    playerBar.width = 505;
    playerBar.height = 50;
    playerBar.x = 10;
    playerBar.y = 10;
    Rectangle playerBarBack;
    playerBarBack.width = 505;
    playerBarBack.height = 50;
    playerBarBack.x = 10;
    playerBarBack.y = 10;
    
    Rectangle enemyBar;
    enemyBar.width = 505;
    enemyBar.height = 50;
    enemyBar.x = screenWidth - enemyBar.width - 10;
    enemyBar.y = 10;
    Rectangle enemyBarBack;
    enemyBarBack.width = 505;
    enemyBarBack.height = 50;
    enemyBarBack.x = screenWidth - enemyBarBack.width - 10;
    enemyBarBack.y = 10;
    
    
    InitAudioDevice();      // Initialize audio device

    Sound hit = LoadSound("resources/paredes.ogg");         // Load WAV audio file
    Sound goal = LoadSound("resources/ganopunto.ogg"); 
    Sound hitPlayer = LoadSound("resources/choquepala.ogg"); 
    Music music = LoadMusicStream("resources/music.ogg");
    Music musicGameplay = LoadMusicStream("resources/music_gameplay.ogg");
    Music musicEndingLose= LoadMusicStream("resources/lose.ogg");
    Music musicEndingWin= LoadMusicStream("resources/win.ogg");
    Music musicEndingDraw= LoadMusicStream("resources/draw.ogg");
    
    
    bool pause = false;
    
    
    PlayMusicStream(music);
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    Texture2D logo = LoadTexture("resources/logo.png");
    
    Color logoColor = WHITE;
    logoColor.a = 0;
    
    Texture2D title = LoadTexture("resources/title.png");
    
    Color titleColor = WHITE;
    titleColor.a = 0;
    
    int pressStart = 1;
    
    bool fadeIn = true;
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    Texture2D logoBackground = LoadTexture("resources/planta.png");
    
    Texture2D logoBackground2 = LoadTexture("resources/planta2.png");
    
    Texture2D gameplayBackground = LoadTexture("resources/fondo.png");
    
    Texture2D winBackground = LoadTexture("resources/win.png");
    
    Texture2D loseBackground = LoadTexture("resources/lose.png");
    
    Texture2D drawBackground = LoadTexture("resources/draw.png");
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                UpdateMusicStream(music);
                
                if (fadeIn)
                {
                    if (logoColor.a < 255)
                    {
                        logoColor.a ++;
                    }
                    else
                    {
                        framesCounter++;
                        
                        if (framesCounter >= 1)
                        {
                            fadeIn = false;
                            framesCounter = 0;
                        }
                            
                    }
                }
                else
                {
                    if (logoColor.a > 0)
                    {
                        logoColor.a --;
                    }
                    else
                    {
                        screen = TITLE;
                    }
                }
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                UpdateMusicStream(music);
                // TODO: Title animation logic.......................(0.5p)
                if (titleColor.a < 255)
                {
                    titleColor.a ++;
                }
                else
                {
                    framesCounter++;
                    if (framesCounter >= 40)
                    {
                        pressStart++;
                        framesCounter = 0;
                    }
                    
                                    
                    if (IsKeyPressed(KEY_ENTER))
                    {
                        screen = GAMEPLAY;
                        framesCounter = 0;
                        PlayMusicStream(musicGameplay);
                    }

                }
                
                // TODO: "PRESS ENTER" logic.........................(0.5p)

                
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
                if (!pause)
                {
                    // TODO: Ball movement logic.........................(0.2p)
                    UpdateMusicStream(musicGameplay);
                    
                    ballPosition.x -= ballSpeed.x;
                    ballPosition.y += ballSpeed.y;
                    

                    
                    // TODO: Player movement logic.......................(0.2p)
                    if (IsKeyDown(KEY_DOWN))
                    {
                        player.y += playerSpeedY;
                    }
                    
                    if (IsKeyDown(KEY_UP))
                    {
                        player.y -= playerSpeedY;
                    }
                    
                    if (player.y <= 90)
                        player.y = 90;
                    else if (player.y  + player.height >= screenHeight)
                    {
                        player.y = screenHeight - player.height;
                    }
                    
                    // TODO: Enemy movement logic (IA)...................(1p)
                    
                    if (ballPosition.x > screenWidth/2)
                    {
                        if (ballPosition.y > enemy.y)
                        {
                            enemy.y+= enemySpeedY;
                        }
                        else
                        {
                            enemy.y-= enemySpeedY;
                        }
                        
                    }
                    
                    if (enemy.y <= 90)
                        enemy.y = 90;
                    else if (enemy.y  + enemy.height >= screenHeight)
                    {
                        enemy.y = screenHeight - enemy.height;
                    }
                    
                    // TODO: Collision detection (ball-player) logic.....(0.5p)
                    
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, player))
                    {
                        ballSpeed.x *= -1;
                        ballPosition.x = player.x + player.width + ballRadius;
                        PlaySound(hitPlayer);
                    }
                    
                    // TODO: Collision detection (ball-enemy) logic......(0.5p)
                    
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, enemy))
                    {
                        ballSpeed.x *= -1;
                        ballPosition.x = enemy.x - ballRadius;
                        PlaySound(hitPlayer);
                    }
                    
                    // TODO: Collision detection (ball-limits) logic.....(1p)
                    if (ballPosition.y + ballRadius >= screenHeight || ballPosition.y - ballRadius <= 90)
                    {
                        ballSpeed.y *= -1;
                        PlaySound(hit);
                    }
                    
                    // TODO: Life bars decrease logic....................(1p)
                    if (ballPosition.x + ballRadius >= screenWidth)
                    {
                        ballSpeed.x *= -1;
                        ballSpeed.x *= -1;
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        enemyBar.width -= 60;
                        enemyBar.x += 60;
                        PlaySound(goal);
                    }
                    else if (ballPosition.x - ballRadius <= 0)
                    {
                        ballSpeed.x *= -1;
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        playerBar.width -= 60;
                        PlaySound(goal);
                    }

                    // TODO: Time counter logic..........................(0.2p)
                    framesCounter++;
                    
                    if (framesCounter >= 60)
                    {
                        framesCounter = 0;
                        secondsCounter--;
                    }

                    // TODO: Game ending logic...........................(0.2p)    
                    if (playerBar.width <= 0)
                    {
                        gameResult = -1;
                        screen = ENDING;
                        PlayMusicStream(musicEndingLose);
                    }
                    
                    if (enemyBar.width <= 0)
                    {
                        gameResult = 1;
                        screen = ENDING;
                        PlayMusicStream(musicEndingWin);
                    }
                    
                    if (secondsCounter <= 0)
                    {
                        gameResult = 0;
                        screen = ENDING;
                        PlayMusicStream(musicEndingDraw);
                    }
                }
                
                // TODO: Pause button logic..........................(0.2p)
                
                if (IsKeyPressed(KEY_ENTER))
                {
                    pause = !pause;
                }
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                UpdateMusicStream(musicEndingLose);
                
                if (gameResult == -1)
                {
                    UpdateMusicStream(musicEndingLose);
                }
                else if (gameResult == 0)
                {
                    UpdateMusicStream(musicEndingDraw);
                }
                else if(gameResult == 1)
                {
                    UpdateMusicStream(musicEndingWin);
                }
                
                if (IsKeyPressed(KEY_ENTER))
                {
                    playerBar.width = 520;
                    enemyBar.width = 520;
                    enemyBar.x = screenWidth - enemyBar.width - 10;
                    secondsCounter = 99;
                    ballPosition.x = screenWidth/2;
                    ballPosition.y = screenHeight/2;
                    player.y = screenHeight/2 - player.height/2;
                    enemy.y = screenHeight/2 - enemy.height/2;
                    ballSpeed.x = 10;
                    ballSpeed.y = 10;
                    screen = GAMEPLAY;
                }
                
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
                
            ClearBackground(WHITE);
            
           
            switch(screen) 
            {
                case LOGO: 
                {
                    
                    DrawTexture(logoBackground, 0, 0, WHITE);
                    
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    
                    DrawTexture(logo, screenWidth/2 - logo.width/2, screenHeight/2 - logo.height/2, logoColor);
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    DrawTexture(logoBackground2, 0, 0, WHITE);
                    
                    // TODO: Draw Title..............................(0.2p)
                    DrawTexture (title, screenWidth/2 - title.width/2, screenHeight/3 - title.height/2, titleColor);
                    
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    
                    if (pressStart % 2 == 0)
                        DrawText ("PRESS START", screenWidth/2 - MeasureText("PRESS START", 50)/2, screenHeight/3 * 2, 50, WHITE);
                    
                } break;
                case GAMEPLAY:
                {     
                
                  DrawTexture(gameplayBackground, 0, 0, WHITE);
                
                    // Draw GAMEPLAY screen here!
                    //DrawText("Estamos en el gameplay", 200, 200, 50, BLACK);
                    DrawCircleV (ballPosition, ballRadius, BLACK);
                    // TODO: Draw player and enemy...................(0.2p)
                    DrawRectangleRec (player, BLACK);
                    DrawRectangleRec (enemy, BLACK);
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    
                    DrawRectangleRec (playerBarBack, RED);
                    DrawRectangleRec (playerBar, GREEN);
                    
                    DrawRectangleRec (enemyBarBack, RED);
                    DrawRectangleRec (enemyBar, GREEN);
                    
                    // TODO: Draw time counter.......................(0.5p)
                    
                    DrawText (FormatText("%02i", secondsCounter), screenWidth/2 - MeasureText("00", 50)/2, 24, 50, BLACK);
                    
                    // TODO: Draw pause message when required........(0.5p)
                    
                    if (pause)
                        DrawText ("PAUSE", screenWidth/2 - MeasureText("PAUSE", 50)/2, screenHeight/2, 50, BLACK);
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    //DrawTexture(loseBackground, 0, 0, WHITE);
   
                    //DrawTexture(drawBackground, 0, 0, WHITE);
                    
                   // DrawTexture(winBackground, 0, 0, WHITE);
                    
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                    if (gameResult == -1)
                    {
                        DrawTexture(loseBackground, 0, 0, WHITE);
                       // DrawText ("YOU LOSE", screenWidth/2 - MeasureText("YOU LOSE", 50)/2, screenHeight/2, 50, RED);
                    }
                    else if (gameResult == 0)
                    {
                        DrawTexture(drawBackground, 0, 0, WHITE);
                       // DrawText ("DRAW GAME", screenWidth/2 - MeasureText("DRAW GAME", 50)/2, screenHeight/2, 50, PINK);
                    }
                    else
                    {
                        DrawTexture(winBackground, 0, 0, WHITE);
                       // DrawText ("YOU WIN", screenWidth/2 - MeasureText("YOU WIN", 50)/2, screenHeight/2, 50, BLUE);
                    }
                    
                } break;
                default: break;
            }
        
            DrawFPS(565, 0);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    UnloadSound(hit);
    UnloadSound(goal);
    UnloadSound(hitPlayer);
    UnloadMusicStream(music);
    UnloadMusicStream(musicGameplay);
    UnloadMusicStream(musicEndingLose);
    UnloadMusicStream(musicEndingWin);
    UnloadMusicStream(musicEndingDraw);
    UnloadTexture(title);
    UnloadTexture(logoBackground);
    UnloadTexture(gameplayBackground);
    UnloadTexture(winBackground);
    UnloadTexture(loseBackground);
    UnloadTexture(drawBackground);
    
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}